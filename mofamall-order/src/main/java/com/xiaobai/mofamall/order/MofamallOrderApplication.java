package com.xiaobai.mofamall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MofamallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(MofamallOrderApplication.class, args);
    }

}

package com.xiaobai.mofamall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MofamallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(MofamallMemberApplication.class, args);
    }

}

package com.xiaobai.mofamall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MofamallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(MofamallCouponApplication.class, args);
    }

}

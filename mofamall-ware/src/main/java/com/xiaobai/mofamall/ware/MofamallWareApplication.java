package com.xiaobai.mofamall.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MofamallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(MofamallWareApplication.class, args);
    }

}

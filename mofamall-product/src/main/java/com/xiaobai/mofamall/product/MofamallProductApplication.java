package com.xiaobai.mofamall.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MofamallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(MofamallProductApplication.class, args);
    }

}
